// ====================================================================
// Copyright Stephane MOTTELET 2023
// This file is released into the public domain
// ====================================================================

if atomsIsInstalled("sci_sundials")
    mprintf("Sundials solvers are part of Scilab, removing module:\n");
    disp(atomsRemove("sci_sundials"))
    mprintf("\nPlease restart Scilab.\n");
end
